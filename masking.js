const path = require('path');
require('dotenv').config({ path: path.resolve('.env') });

const knex = require('knex')({
  client: 'mysql',
  connection: {
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT, 10),
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_TEMP
  }
});

const fs = require('fs');
const csv = require('csv-parser');

const readCsv = async (inputFilePath) => {
  const list = [];
  return new Promise((res) => {
    fs.createReadStream(inputFilePath)
      .pipe(csv())
      .on('data', function (data) {
        try {
          const columns = Object.values(data);
          columns.length && list.push(columns);
        }
        catch (err) {
          //error handler
        }
      })
      .on('end', function () {
        const map = [];
        for (let i = 0; i < list.length; i+=2) {
          const table = list[i];
          const columns = list[i+1];
          map.push({
            table: table[0], columns
          })
        }
        res(map);
      });
  })
}

const readRandomCsv = async (inputFilePath) => {
  const list = [];
  return new Promise((res) => {
    fs.createReadStream(inputFilePath)
      .pipe(csv())
      .on('data', function (data) {
        try {
          const columns = Object.values(data);
          columns.length && list.push(columns);
        }
        catch (err) {
          //error handler
        }
      })
      .on('end', function () {
        const map = [];
        for (let i = 0; i < list.length; i+=3) {
          const table = list[i];
          const columns = list[i+1];
          const range = list[i+2];
          map.push({
            table: table[0], columns, range
          })
        }
        res(map);
      });
  })
}

const isTableExists = async (table) => {
  try {
    await knex(table).count('*');
    
    return true;
  } catch(err) {
    return false;
  }
}

const updateTextField = async (table, columns) => {
  if (!columns || columns.length === 0) return Promise.resolve(0);

  const foundTable = await isTableExists(table);
  if (!foundTable) {
    console.error('Can not found table ', table);
    return Promise.resolve(0);
  }

  const updatedFields = columns.reduce((acc, it) => {
    acc[it] = knex.raw(`concat("${it}", " - ", id)`)
    return acc;
  }, {});
  return knex(table).update(updatedFields).debug();
};

const updateWithRandomValue = async (table, column, min, max) => {
  const updatedFields = [column].reduce((acc, it) => {
    acc[column] = knex.raw(`ROUND(RAND()*(${max}-${min})+${min})`)
    return acc;
  }, {});
  return knex(table).update(updatedFields).debug();
};

const updateWithValue = async (table, column, value) => {
  const updatedFields = [column].reduce((acc, it) => {
    acc[column] = value
    return acc;
  }, {});
  return knex(table).update(updatedFields).debug();
};

async function main() {
  try {
    // Dummy text
    const schemaMasking = await readCsv('schema/schema-masking.csv');
    await Promise.all(schemaMasking.map(({table, columns}) => updateTextField(table, columns)));

    // Random number
    const schemaRandom = await readRandomCsv('schema/schema-random.csv');
    await Promise.all(schemaRandom.map(({table, columns, range}) => updateWithRandomValue(table, columns, range[0], range[1])));
    
    // Dummy password and email
    await updateWithValue('user', 'password', process.env.MASKING_DUMMY_PASSWORD); // the private password
    await updateWithValue('user', 'email', knex.raw(`concat("user_", id, ${process.env.MASKING_DUMMY_EMAIL})`));

    console.log('==== SUCCESS ====');
    process.exit(0);
  } catch (error) {
    console.error('---- Failed to run mirror script with error: ', error);
    process.exit(1);
  }
}

main();
