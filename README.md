### Installation
- nvm && NodeJS 12

```
yarn
```

### Executing
```bash
cp .env.example .env
mysql-masking.sh
```

### What in this script
```
mysql-masking.sh
```

- 1. Export db schema
- 2. Export db data (excludes selected tables) to .sql file
- 3. Create `temp` db and import data from generated .sql file
- 4. Masking data in `temp` db and export .sql file
- 5. Import masking .sql file to target db
- 6. Tar masking data file to `data` and cleanup .sql files


### Dummy data guide
- Dummy text
    - data/schema-masking.csv
    - strucs
        - Row 1: table name
        - Row 2: array of applied columns

- Random number
    - data/schema-random.csv
    - strucs
        - Row 1: table name
        - Row 2: array of applied columns
        - Row 3: [min,max] values


- Run masking data
```
node masking.js
```