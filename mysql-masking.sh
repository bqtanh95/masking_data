#!/bin/sh
source .env

export DUMP_FILE=$FILE_NAME-"$(date +%Y%m%d)".sql
export MASKED_FILE=$FILE_NAME-masked-"$(date +%Y%m)".sql

echo "1. EXPORTING... db to file $DUMP_FILE"

mysqldump -u $DB_USER -h $DB_HOST otable --password=$DB_PASS --set-gtid-purged=OFF --no-data --column-statistics=0 > $DUMP_FILE

echo "2. DUMPING data"
IGNORED_TABLES_STRING=''
for TABLE in "${EXCEPT_SCHEMA[@]}"
do :
   IGNORED_TABLES_STRING+=" --ignore-table=${DB_NAME}.${TABLE}"
done
echo "Dumping except schema $IGNORED_TABLES_STRING"

mysqldump -u $DB_USER -h $DB_HOST otable --password=$DB_PASS --set-gtid-purged=OFF --column-statistics=0 --no-create-info $IGNORED_TABLES_STRING >> $DUMP_FILE

echo "3. IMPORTING TEMP DB"
echo "3.1 DROP DATABASE IF EXISTS $DB_TEMP"
echo "DROP DATABASE IF EXISTS $DB_TEMP;" | mysql -u $DB_USER -h $DB_HOST --password=$DB_PASS

echo "3.2 CREATE DATABASE $DB_TEMP"
echo "CREATE DATABASE $DB_TEMP;" | mysql -u $DB_USER -h $DB_HOST --password=$DB_PASS

echo "3.3 Importing... $DUMP_FILE to $DB_TEMP"
mysql -u $DB_USER -h $DB_HOST $DB_TEMP --password=$DB_PASS < $DUMP_FILE

echo "4. MASKING DATA...in db $DB_TEMP"
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
nvm use
echo "4.1 Masking data in db $DB_TEMP"
node masking.js

echo "4.2 Exporting... masking data to $MASKED_FILE"
mysqldump --set-gtid-purged=OFF --column-statistics=0 --no-set-names --compact -u $DB_USER -h $DB_HOST $DB_TEMP --password=$DB_PASS > $MASKED_FILE

echo "5. IMPORTING MASKED DATA TO TARGET DB"
echo "5.1 DROP DATABASE IF EXISTS $DB_TARGET_NAME"
echo "DROP DATABASE IF EXISTS $DB_TARGET_NAME;" | mysql -u $DB_TARGET_USER -h $DB_TARGET_HOST --password=$DB_TARGET_PASS

echo "5.2 CREATE DATABASE $DB_TARGET_NAME"
echo "CREATE DATABASE $DB_TARGET_NAME;" | mysql -u $DB_TARGET_USER -h $DB_TARGET_HOST --password=$DB_TARGET_PASS

echo "5.3 Disable foreign key check"
printf '%s\n%s\n' "SET FOREIGN_KEY_CHECKS=0;" "$(cat $MASKED_FILE)" > $MASKED_FILE

echo "5.4 Import db ... get masking data from $MASKED_FILE"
mysql -u $DB_TARGET_USER -h $DB_TARGET_HOST --password=$DB_TARGET_PASS $DB_TARGET_NAME < $MASKED_FILE

echo "6. Cleaning up..."
tar zcvf "./data/$MASKED_FILE".tgz  $MASKED_FILE
rm -f $DUMP_FILE
rm -f $MASKED_FILE

echo "Delete *.sql files older than 60 days"
find ./*.sql* -mindepth 0 -mtime +60 -delete

echo "====DONE==="
echo ''
